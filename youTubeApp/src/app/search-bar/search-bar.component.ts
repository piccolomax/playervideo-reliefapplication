import { Component, OnInit } from '@angular/core';

import { HistoryService } from '../history.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  constructor(public historyService: HistoryService) { }

  tmpUrl = "";

  ngOnInit() {
  }

  logForm(): void {
    this.historyService.addHistory(this.tmpUrl);
    this.tmpUrl = "";
  }
}
