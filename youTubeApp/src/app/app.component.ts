import { Component } from '@angular/core';

import { HistoryService } from './history.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showBookmark = false;

  title = 'Youtube player';

  constructor(private historyService: HistoryService) { }

  changeBookmarkFlag() {
    if (!this.showBookmark)
      this.showBookmark = true;
    else
      this.showBookmark = false;
  }

}
