import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class HistoryService {
  histories: any = JSON.parse(localStorage.getItem('histories'));
  bookmarks: any = JSON.parse(localStorage.getItem('bookmarks'));
  selectedUrl: any;
  showBookmark = false;
  nbBookmarks = JSON.parse(localStorage.getItem('bookmarks'))
  ? JSON.parse(localStorage.getItem('bookmarks')).length : 0;

  constructor(public sanitizer: DomSanitizer) { }

  addHistory(url: string): void {
    if (url) {
      if (!this.histories)
        this.histories = [];
      url = url.replace("watch?v=", "embed/");
      if (!this.strncmp("https://www.youtube.com/embed/", url, 30)
        && !this.checkDuplicateItem(this.histories, url)) {
       this.selectedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        this.histories.push(url);
        localStorage.setItem('histories', JSON.stringify(this.histories));
      }
    }
  }

  changeSelection(url: string): void {
   this.selectedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  addBookmarks(): void {
    if (this.selectedUrl) {
      if (!this.bookmarks)
        this.bookmarks = [];
      if (!this.checkDuplicateItem(this.bookmarks, this.selectedUrl.changingThisBreaksApplicationSecurity)) {
        this.bookmarks.push(this.selectedUrl.changingThisBreaksApplicationSecurity);
        localStorage.setItem('bookmarks', JSON.stringify(this.bookmarks));
        this.nbBookmarks = JSON.parse(localStorage.getItem('bookmarks')).length;
      }
    }
  }

  strncmp(str1, str2, n) {
    str1 = str1.substring(0, n);
    str2 = str2.substring(0, n);
    return (( str1 == str2 ) ? 0 : (( str1 > str2 ) ? 1 : -1 ));
  }

  checkDuplicateItem(haystack: string[], needle: string) {
    for(var i = 0; i < haystack.length; i++)
      if (haystack[i] == needle)
        return (1);
    return (0);
  }

}
