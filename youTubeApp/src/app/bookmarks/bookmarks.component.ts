import { Component, OnInit } from '@angular/core';

import { HistoryService } from '../history.service';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {

  constructor(private historyService: HistoryService) { }

  ngOnInit() {
  }

}
